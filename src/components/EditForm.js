import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import axios from "axios";

const api = axios.create({
  baseURL: `http://110.74.194.124:15011/v1/api/articles`,
});

function EditForm({ match }) {
  useEffect(() => {
    getArticle();
  }, []);

  const initState = {
    title: "",
    description: "",
    image: "",
  };

  const [article, setCurrArticle] = useState(initState);

  const getArticle = () => {
    api.get("/" + match.params.id).then((res) => {
      //   setCurrArticle(res.data.DATA);
      const data = res.data.DATA;
      let temp = {
        title: data.TITLE,
        description: data.DESCRIPTION,
        image: data.IMAGE,
      };

      setCurrArticle(temp);

      //   const fetched = res.data.DATA;
      //   setArticle(fetched);
    });
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setCurrArticle({ ...article, [name]: value });
  };

  let history = useHistory();

  let checkImage = (i) => {
    if (i === null || i === "" || i.startsWith("blob")) {
      return "https://2unb4qg7x653wubo12zfuaxe-wpengine.netdna-ssl.com/wp-content/uploads/2017/05/placeholder-image-cropped.jpg";
    } else {
      return i;
    }
  };

  const submitArticle = (e) => {
    e.preventDefault();
    const updated = {
      TITLE: article.title,
      DESCRIPTION: article.description,
      IMAGE: checkImage(article.image),
    };

    axios
      .put(
        `http://110.74.194.124:15011/v1/api/articles/${match.params.id}`,
        updated
      )
      .then((res) => {
        alert(res.data.MESSAGE);
        history.push("/");
      });
  };

  return (
    <div className="container mt-4">
      <h3>Update Article</h3>
      <form onSubmit={submitArticle}>
        <div className="row">
          <div className="col-lg-8">
            <div>
              <div>
                <label htmlFor="">Title:</label>
              </div>
              <input
                type="text"
                name="title"
                className="form-control"
                placeholder="Input Title Here"
                value={article.title}
                onChange={handleInputChange}
                required
              />
            </div>
            <div>
              <div>
                <label htmlFor="">Description:</label>
              </div>
              <input
                type="text"
                name="description"
                className="form-control"
                placeholder="Input Description Here"
                value={article.description}
                onChange={handleInputChange}
                required
              />
            </div>

            <div>
              <div>
                <label htmlFor="">Image URL:</label>
              </div>
              <input
                type="url"
                name="image"
                className="form-control"
                placeholder="Input Image URL Here"
                value={article.image}
                onChange={handleInputChange}
              />
            </div>

            <div className="mt-3">
              <input type="submit" value="Submit" className="btn btn-primary" />
            </div>
          </div>
          <div className="col-lg-4">
            <img
              src={checkImage(article.image)}
              alt=""
              className="img-fluid mt-4"
            />
          </div>
        </div>
      </form>
    </div>
  );
}

export default EditForm;
