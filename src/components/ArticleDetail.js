import React, { useState, useEffect } from "react";
import axios from "axios";

const api = axios.create({
  baseURL: `http://110.74.194.124:15011/v1/api/articles/`,
});

function ArticleDetail({ match }) {
  const [article, setArticle] = useState({});

  useEffect(() => {
    getArticle();
  }, []);

  const getArticle = () => {
    api.get("/" + match.params.id).then((res) => {
      setArticle(res.data.DATA);

      //   const fetched = res.data.DATA;
      //   setArticle(fetched);
    });
  };

  return (
    <div className="container mt-4">
      <img src={article.IMAGE} alt="" className="img-fluid" />
      <hr className="mt-4" />
      <h1>{article.TITLE}</h1>
      <p>{article.DESCRIPTION}</p>
    </div>
  );
}

export default ArticleDetail;
