import React from "react";
import { Link } from "react-router-dom";

function TableList({ articles, loading, deleteArticle }) {
  let dateFormat = (d) => {
    let year = d.substring(0, 4);
    let month = d.substring(4, 6);
    let day = d.substring(6, 8);
    let result = year + "-" + month + "-" + day;
    return result;
  };
  return (
    <div className="container">
      {loading ? (
        <h2 className="text-center">Loading...</h2>
      ) : (
        <div>
          <div className="mt-4">
            <h3 className="text-center">Article Management</h3>
            <div className="text-center mt-3">
              <Link to="/add">
                <button className="btn btn-primary">Add New Article</button>
              </Link>
            </div>
          </div>
          <table className="table table-bordered table-striped mt-4">
            <thead>
              <tr>
                <th>#</th>
                <th>Title</th>
                <th>Description</th>
                <th>Created Date</th>
                <th>Image</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {articles.map((article) => (
                <tr key={article.ID}>
                  <td className="p-2">{article.ID}</td>
                  <td style={{ width: "15%" }} className="p-2">
                    {article.TITLE}
                  </td>
                  <td className="p-2">{article.DESCRIPTION}</td>
                  <td style={{ width: "13%" }} className="p-2">
                    {dateFormat(article.CREATED_DATE)}
                  </td>
                  <td>
                    <img src={article.IMAGE} className="p-2 img-fluid" alt="" />
                  </td>
                  <td style={{ width: "20%" }} className="text-center p-2">
                    <Link to={`/article/${article.ID}`}>
                      <button className="btn btn-primary">View</button>
                    </Link>
                    <Link to={`/edit/${article.ID}`}>
                      <button className="btn btn-warning ml-2">Edit</button>
                    </Link>
                    <button
                      className="btn btn-danger ml-2"
                      onClick={() => {
                        deleteArticle(article.ID);
                      }}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      )}
    </div>
  );
}

export default TableList;
