import React, { useEffect, useState } from "react";
import "./App.css";
import axios from "axios";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import TableList from "./components/TableList";
import ArticleDetail from "./components/ArticleDetail";
import InputForm from "./components/InputForm";
import EditForm from "./components/EditForm";
import Navbar from "./components/Navbar";

const api = axios.create({
  baseURL: `http://110.74.194.124:15011/v1/api/articles`,
});

function App() {
  const [articles, setArticles] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getArticleAll();
  }, [articles]);

  const getArticleAll = () => {
    api.get("/").then((res) => {
      const fetched = res.data.DATA;
      setArticles(fetched);
      setLoading(false);
    });
  };

  const deleteArticle = (id) => {
    axios
      .delete(`http://110.74.194.124:15011/v1/api/articles/${id}`)
      .then((res) => {
        alert(res.data.MESSAGE);
        getArticleAll();
      });
  };

  return (
    <div>
      <Router>
        <Navbar />
        <Switch>
          <Route
            path="/"
            exact
            render={() => (
              <TableList
                articles={articles}
                loading={loading}
                deleteArticle={deleteArticle}
              />
            )}
          />
          <Route path="/add" exact component={InputForm} />
          <Route path="/edit/:id" exact component={EditForm} />
          <Route path="/article/:id" exact component={ArticleDetail} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
